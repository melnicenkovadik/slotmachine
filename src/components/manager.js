
export default class Manager {
  constructor() {
    this.width = null;
    this.height = null;
    this.stopCount = 0;
    this.result = [];
    this.stage = new PIXI.Container();
  }


  createRenderer() {
    this.renderer = PIXI.autoDetectRenderer(this.width, this.height);

    document.body.appendChild(this.renderer.view);

    this.renderer.render(this.stage);

  }

  firstScreen() {
    let startPopup = new PIXI.Container(),
      wrapper = new PIXI.Graphics(),
      width = 960, height = 535;

    wrapper.lineStyle(2, 0x0000FF, 1);
    wrapper.beginFill(0x000000, 0.7);
    wrapper.drawRect(0, 0, width, height);

    let style = {
      fontFamily: 'Arial',
      fontWeight: 'bold',
      fontSize: '46px',
      fill: '#60baf2',
      stroke: '#4a1850',
      strokeThickness: 5,
      wordWrap: true,
      wordWrapWidth: 440
    };
    let text = new PIXI.Text('Loading..', style);
    text.anchor.set(0.5);
    text.x = width / 2;
    text.y = height / 2;

    startPopup.addChild(wrapper);
    startPopup.addChild(text);
    this.button.deactivate();

    this.stage.addChild(startPopup);
    setTimeout(() => {
      this.deleteIt(startPopup);
      this.button.activate();

    }, 2000);
  }

  //добавление обьектов на сцену
  stageAdd(item) {
    this.stage.addChild(item);
    this.renderer.render(this.stage);
  }

  deleteIt(item) {
    this.stage.removeChild(item);
    this.renderer.render(this.stage);
  }

  onSpeen() {
    this.stopCount = 0;
    this.rating.decrease();
    this.drums.rotate(this.onEndAnimation.bind(this));
  }

  onEndAnimation() {
    this.stopCount++;
    if (this.stopCount === 3) {
      this.button.activate();
      this.rating.checkForWin(this.drums.globalResult);
      this.emptyCouns();
    }

  }

  emptyCouns() {
    if (this.rating.coins <= 3) {
      this.button.deactivate();
      this.rating.refillAcount();
    }
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));
    this.renderer.render(this.stage);
  }
}