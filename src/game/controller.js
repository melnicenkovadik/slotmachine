import Manager from '../components/manager';

import Loader from './Loader';
import Drums from './Drums';
import SpeenButton from './SpeenButton';
import Rating from './Rating';
import Fond from './Fond';

export default class Controller extends Manager {
  constructor() {
    super();
    this.width = 960;
    this.height = 535;

    this.createRenderer();

    new Loader(this.mounted.bind(this));

  }

  mounted() {

    this.createScene();
    this.createDrums();
    this.createButton();
    this.createRating();
    this.animate();
    this.firstScreen();
  }

  //создание сцены
  createScene() {
    this.font = new Fond();
    this.stageAdd(this.font.container);
  }

  //создание барабанов
  createDrums() {

    this.drums = new Drums(this.stageAdd.bind(this));
    this.stageAdd(this.drums.container);
  }

  //создание кнопки
  createButton() {
    this.button = new SpeenButton(this.onSpeen.bind(this));
    this.stageAdd(this.button.container);

  }

  //создание рейтинга
  createRating() {
    this.rating = new Rating();
    this.stageAdd(this.rating.container);
  }

}